# Technical Test Telemaque
Laravel 9.19 and PHP 8.0.2
## First Part : Client
First, a Client factory is present, just run the migrtion with the flag --seed to insert 50k Clients in our BDD (This can take a while).

The route to retrieve clients is localhost/clients.
You can add multiple parameters , depends on wich field you want and if you search a particulary birthday

This is an example of a full request for Client

http://localhost/clients?firstname=true&lastname=true&phone=true&birthday=true&searchingBirthday=2008-01

You can set all parameters to false if you don't want it , or just remove it. Except for searchingBirthday, you can't set it to false but you can remove it for the URI.

This will make you download a file call clients.csv and contains all our requested clients


## Second Part compess / uncompress script

This two methods are available at the url 

http://localhost/compress/myText 

and

http://localhost/uncompress/myText 

To compress a text , you can pass by example this string as text parameter :

http://localhost/compress/abbcccffaaz it will return ab2c3f2a2z

To uncompress a text , same way but at this url :

http://localhost/uncompress/ab2c3f2a2z it will return abbcccffaaz

Enjoy !

