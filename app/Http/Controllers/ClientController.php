<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function __invoke(): JsonResponse
    {
        $clients = Client::all();
        return response()->json([
            "success" => true,
            "message" => "Client List",
            "data" => $clients
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function export(Request $request)
    {
        // Get params array
        $params = $request->all();
        // Init required fields
        $requiredFields = [];
        //init birthday search
        $searchingBirthday = null;
        // Check if params present in request
        if (sizeof($params) > 0) {
            // Push each required params into $requiredFields and set $searchingBirthday if necessary
            foreach ($params as $key => $field) {
                if ($key !== 'searchingBirthday') {
                    if ($field === 'true') {
                        $requiredFields[] = $key;
                    }
                } else {
                    $searchingBirthday = $field;
                }
            }
            // Retrieve Clients based on the required fields
            $clients = Client::select($requiredFields)->where('birthday', 'like', "%$searchingBirthday%")->get();
        } else {
            // No required fields , so retrieve all Clients
            $clients = Client::all();
        }
        // Send CSV file
        return fastexcel($clients)->download('clients.csv');
    }

}

