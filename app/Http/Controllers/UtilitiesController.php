<?php

namespace App\Http\Controllers;


class UtilitiesController extends Controller
{
    public function compress($text): string
    {
        // Transform string into array
        $arr = str_split($text);
        // init return string
        $newText = '';
        // init amount of the same letter
        $counter = 1;

        // Go threw each entries of our array
        foreach ($arr as $key => $letter) {
            // Check if next letter's index is lower than array length -1 and if the actual letter is same as next letter
            if (($key + 1 <= count($arr) - 1) && ($letter === $arr[$key + 1])) {
                // if yes, add 1 to our counter
                $counter++;
            } else {
                // if no, check the counter value
                if ($counter > 1) {
                    //more than 1 so add the letter and amount of entries to our final string
                    $newText = $newText . $letter . $counter;
                } else {
                    // else add the lonely letter to our final string
                    $newText = $newText . $letter;
                }
                //reset the counter to 1
                $counter = 1;
            }
        }
        return $newText;
    }

    public function uncompress($text): string
    {
        // Transform string into array
        $arr = str_split($text);
        // init return string
        $newText = '';
        //init amount of letters
        $amount = null ;

        // Go threw each entries of our array
        foreach ($arr as $key => $letter) {
            // Check if next entry is lower or equal than array length -1 and if next entry is a numeric
            if (($key + 1 <= count($arr) - 1) && (is_numeric($arr[$key + 1]))) {
                // Yes so add the numeric to our amount of letters required
                $amount = $amount . $arr[$key + 1];
            } else {
                // No so check if amount is null
                if ($amount !== null) {
                    // Amount is not null so we add as many letters as your amount
                    for ($i = 0; $i < $amount; $i++ ) {
                        $newText = $newText . $arr[$key - strlen($amount)];
                    }
                } else {
                    // Amount is null so we juste add the letter
                    $newText = $newText . $letter;
                }
                // Reset amount to null
                $amount = null;
            }
        }
        return $newText;
    }
}
