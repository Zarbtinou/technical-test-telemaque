<?php

use App\Http\Controllers\UtilitiesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('clients/all', [ClientController::class, '__invoke']);
Route::get('clients/{firstname?}/{lastname?}/{phone?}/{birthday?}', [ClientController::class, 'export']);
Route::get('compress/{text}', [UtilitiesController::class, 'compress']);
Route::get('uncompress/{text}', [UtilitiesController::class, 'uncompress']);
